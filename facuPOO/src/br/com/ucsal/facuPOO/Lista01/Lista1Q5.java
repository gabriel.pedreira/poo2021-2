package br.com.ucsal.facuPOO.Lista01;

public class Lista1Q5 {

	public static void main(String[] args) {
		
		imprimeValoresPA(geradorDePA(2, 10, 5));
		
		imprimeSomaPA(somadorDePA(geradorDePA(2, 10, 5)));

	}

	public static int[] geradorDePA(int tamanho, int primeiroTermo, int razao) {

		int[] pA = new int[tamanho];

		for (int i = 0; i < pA.length; i++) {

			if (i == 0) {

				pA[i] = primeiroTermo + razao;

			} else {

				pA[i] = pA[i - 1] + razao;

			}

		}

		return pA;

	}

	public static int somadorDePA(int[] pA) {

		int soma = 0;

		for (int i = 0; i < pA.length; i++) {

			if (i == 0) {

				soma = pA[i];

			} else {

				soma += pA[i];

			}

		}

		return soma;

	}

	public static void imprimeValoresPA(int[] pA) {

		System.out.println("Os valores presentes na PA s�o: ");

		for (int i = 0; i < pA.length; i++) {

			System.out.print(pA[i] + "  ");

		}
		
		System.out.println("");

	}

	public static void imprimeSomaPA(int soma) {

		System.out.println("O valore da soma da PA foi de : " + soma);

	}

}
