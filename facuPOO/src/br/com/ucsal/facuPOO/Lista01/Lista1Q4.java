package br.com.ucsal.facuPOO.Lista01;

import java.util.Scanner;

public class Lista1Q4 {

	public static void main(String[] args) {
		
		imprimeResultado(comparadorDeDados(guardaNomes(geraArrayStringUni10()), guardaAlturas(geraArrayDoubleUni10()), guardaPesos(geraArrayDoubleUni10())));

	}

	public static double[] geraArrayDoubleUni10() {

		double[] arrayuni10 = new double[10];

		return arrayuni10;

	}

	public static String[] geraArrayStringUni10() {

		String[] arrayuni10 = new String[10];

		return arrayuni10;

	}

	public static String[] guardaNomes(String[] nomes) {

		Scanner coletaNomes = new Scanner(System.in);

		for (int i = 0; i < nomes.length; i++) {

			System.out.println("Nome da pessoa " + i + " : ");

			nomes[i] = coletaNomes.nextLine();

		}

		return nomes;

	}

	public static double[] guardaAlturas(double[] altura) {

		Scanner coletaAltura = new Scanner(System.in);

		for (int i = 0; i < altura.length; i++) {

			System.out.println("Altura da pessoa " + i + " : ");

			altura[i] = coletaAltura.nextDouble();

		}

		return altura;

	}

	public static double[] guardaPesos(double[] peso) {

		Scanner coletaPeso = new Scanner(System.in);

		for (int i = 0; i < peso.length; i++) {

			System.out.println("Peso da pessoa " + i + " : ");

			peso[i] = coletaPeso.nextDouble();

		}

		return peso;

	}

	public static String[] comparadorDeDados(String[] nomes, double[] alturas, double[] pesos) {

		String[] resultados = new String[2];

		double maiorAltura = 0, maiorPeso = 0;

		int marcador1 = 0, marcador2 = 0;

		for (int i = 0; i < nomes.length; i++) {

			if (alturas[i] > maiorAltura) {

				maiorAltura = alturas[i];

				marcador1 = i;

			}

		}

		for (int i = 0; i < nomes.length; i++) {

			if (pesos[i] > maiorAltura) {

				maiorPeso = pesos[i];

				marcador2 = i;

			}

		}

		resultados[0] = "o resultado do mais pesado foi: " + nomes[marcador2] + " Peso: " + maiorPeso;

		resultados[1] = "o resultado do mais alto foi: " + nomes[marcador1] + " Altura: " + maiorAltura;

		return resultados;

	}
	
	public static void imprimeResultado(String [] resultado) {
		
		for (int i = 0; i < resultado.length; i++) {
			
			System.out.println(resultado[i]);
			
		}
		
	}
	

}
