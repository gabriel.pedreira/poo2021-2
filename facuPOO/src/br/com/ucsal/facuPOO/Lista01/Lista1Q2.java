package br.com.ucsal.facuPOO.Lista01;

import java.util.Scanner;

public class Lista1Q2 {

	public static void main(String[] args) {

		imprimeE(calculaValorE(coletaN()));

	}

	public static int coletaN() {

		Scanner coletaValor = new Scanner(System.in);

		int n = 0;

		System.out.println("declare o valor de N, sendo ele inteiro e positivo");

		n = coletaValor.nextInt();

		return n;

	}

	public static int realizaFatorial(int n) {

		int fatorial = 1;

		for (int i = 0; i < n; i++) {

			fatorial *= (i + 1);

		}

		return fatorial;

	}

	public static double calculaValorE(int n) {

		double e = 1, fatorial;

		for (int i = 0; i < n; i++) {
			fatorial = realizaFatorial(n - i);

			e += 1 / fatorial;

		}

		return e;

	}

	public static void imprimeE(double e) {

		System.out.println(e);

	}

}
