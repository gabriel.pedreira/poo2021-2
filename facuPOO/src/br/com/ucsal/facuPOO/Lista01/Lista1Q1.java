package br.com.ucsal.facuPOO.Lista01;

import java.util.Scanner;

public class Lista1Q1 {

	public static void main(String[] args) {

		retornaAvaliacao(julgaNota(coletaNotaValida()));

	}

	public static int coletaNotaValida() {

		Scanner coletaNota = new Scanner(System.in);

		int nota = 0;

		boolean verificador = false;


		while(!verificador) {

			System.out.println("Declare a nota do aluno a ser avaliado num valor entre 0 e 100.");

			nota = coletaNota.nextInt();
			
			if (nota >= 0 && nota <=100) {
				
				verificador = true;
				
			}else {
				
				System.out.println("Nota inv�lida, por favor insira um valor condizente com a aplica��o.");
				
			}
		}

		return nota;
	}

	public static int julgaNota(int nota) {

		int avaliador = 0;

			if (nota >= 0 && nota <= 49) {

				avaliador = 1;

			} else if (nota <= 64) {

				avaliador = 2;

			} else if (nota <= 84) {

				avaliador = 3;

			} else if (nota <= 100) {

				avaliador = 4;

			}

		return avaliador;

	}

	public static void retornaAvaliacao(int avaliador) {

		switch (avaliador) {
		case 1:

			System.out.println("Nota considerada: insuficiente.");

			break;

		case 2:

			System.out.println("Nota considerada: regular.");

			break;

		case 3:

			System.out.println("Nota considerada: boa.");

			break;

		case 4:

			System.out.println("Nota considerada: �tima.");

		default:
			break;

		}

	}
	
}
