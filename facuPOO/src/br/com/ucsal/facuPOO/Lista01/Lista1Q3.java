package br.com.ucsal.facuPOO.Lista01;

import java.util.Scanner;

public class Lista1Q3 {

	public static void main(String[] args) {

		int[] valores = coletaNumeros();

		imprimeMedia(geraMedia(valores));

		imprimeMaior(comparadorDeTamanhoMaior(valores));

		imprimeMenor(comparadorDeTamanhoMenor(valores));

	}

	public static int[] coletaNumeros() {

		Scanner coletaValores = new Scanner(System.in);

		int[] numeros = new int[10];

		int contador = 0;

		while (contador < 10) {

			System.out.println("Declare os 10 valores inteiros positivos");

			System.out.println("valor " + contador + ":");

			numeros[contador] = coletaValores.nextInt();

			contador++;
		}

		return numeros;

	}

	public static double geraMedia(int[] valores) {

		double media, soma = 0;

		for (int i = 0; i < valores.length; i++) {

			soma += valores[i];

		}

		media = soma / valores.length;

		return media;

	}

	public static void imprimeMedia(double media) {

		System.out.println("A media dos valores escolhidos �: " + media);

	}

	public static int comparadorDeTamanhoMaior(int[] valores) {

		int maiorValor = 0;

		for (int i = 0; i < valores.length; i++) {

			if (valores[i] > maiorValor) {

				maiorValor = valores[i];

			}

		}

		return maiorValor;

	}

	public static void imprimeMaior(int maiorValor) {

		System.out.println("A media dos valores escolhidos �: " + maiorValor);

	}

	public static int comparadorDeTamanhoMenor(int[] valores) {

		int menorValor = 0;

		for (int i = 0; i < valores.length; i++) {

			if (valores[i] < menorValor) {

				menorValor = valores[i];

			}

		}

		return menorValor;

	}

	public static void imprimeMenor(int maiorValor) {

		System.out.println("A media dos valores escolhidos �: " + maiorValor);

	}

}
